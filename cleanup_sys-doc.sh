#!/bin/bash

#Cleans up sys-doc files on listed assets.
#Used for bug tracking/problem identification for doc_hc.sh

input_file="$1"

if [ $# -lt 1 ]; then
   echo ""
   echo "missing required parameter :                                   "
   echo "Usage: $0 <asset list>                                         "
   echo "                                                               "
   echo "you should not be using this script unless you are doing       "
   echo "bug tracking/problem identification for doc_hc.sh              "
   echo ""
   exit 1
fi

function do_cleanup () {
        ping -w 10 -c 5 $target >/dev/null
        RETVAL=$?
        if [[ $RETVAL == 1 ]]; then
                echo "Tracing network route to $newline $target"
                traceroute -m 12 $target
                echo "WARNING! $newline is DEAD!"
                exit 1
        else
					 ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'if [ ! -f /tmp/*_hc.sh ] && [ ! -f /tmp/hc.txt ];then echo "File not found!" ; fi'
					 if [ $? == 0 ] ; then
					 		echo "Eh, nothing to do on $newline"
					 else        			
                		echo "Cleaning up $newline"
                		ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'rm -rf /tmp/hc.txt /tmp/*_hc.sh'
                		echo "Cleaning up logfiles"
                		ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'rm -rf /tmp*-sysinfo-*.log'
                		echo "ooo shiny! $newline cleaned"
                fi
        fi
}


while read -r line
        do
        newline=`echo $line | awk '{print $1}' | tr '[A-Z]' '[a-z]'`
        target=`nslookup $newline | grep Add | grep -v '#' | awk '{print $2}'`

        case $line in
                        PAT*)
                        echo "$newline is a MC/MCD asset - skipping ..."
                        ;;

                        ZTM*)
                        echo "$newline is a MC/MCD asset - skipping ..."
                        ;;

                        MC*VIO*)
                        echo "$newline is a MC/MCD asset - skipping ..."
                        ;;

                        *XS*)
                        echo "$newline is an IDS machine - skipping ..."
                        ;;

                        *-BC-*)
                        echo "$newline is a Blade enclosure - skipping ..."
                        ;;

                        *)
                        do_cleanup
                        ;;

        esac

done < "$1"

exit 0