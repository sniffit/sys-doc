# !/bin/bash
#
# /\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# /                                                             \
# /     Multi-purpose script for cross platform usage           \
# /     Parts of this script is adapted from hp_getsysinfo.sh   \
# /     Written by Lop Ahmad, Ahmad Zulkarnain                  \
# /	  Co Authored by Abdul Halim, Mohd Hanif						 \
# /	  Script tester Yaacob, Siti Nor Shahida						 \
# /																				 \
# //////////////////////////////////////////////////////////////\
#
# Please read the ChangeLog for updates.
#

# Preconfigured variables go here
input_file="$1"
option_flag="$2"
chm_inm="$3"
basedir=$PWD
logfile=$PWD/logs/$chm_inm/$2-`date +%m-%d-%Y`.log
logdir=$PWD/logs/$chm_inm

# Simple logic and usage
FALSE=0
TRUE=1
error_flag=${FALSE}
export FALSE TRUE error_flag

if [ $# -lt 3 ]; then
   echo ""
   echo "missing required parameter : "
   echo "Usage: $0 <asset list> <options> <change/incident number>"
   echo ""
   echo "Options available:-"
   echo ""
   echo "full         - performs platform specific health checks from supplied list"
   echo "simple       - does a very simple health check to see if assets are up"
   echo "clean			 - cleans up the logfile directory to save space"
   echo ""
   echo "have you read the CHANGELOG.txt lately?"
   echo ""
   exit 1
fi

# Fancy borders from Edmund Wong's script
INFO () {
  echo "----------------------------------------------------------------------- " | tee -a $logfile
  echo "INFO: $@" | tee -a $logfile
}

INFO2 () {
  echo "----------------------------------------------------------------------- " | tee -a $logfile2
  echo "INFO2: $@" | tee -a $logfile2
}

ERROR () {
  echo "ERROR: $@" | tee -a $logfile
  error_flag=${TRUE}
}

# We pump the hc.txt to target machines to enable the script to check for flags
# Also clean up the old hc.txt and reset the flags
flag=`cat $logdir/hc.txt | grep flag | cut -d '=' -f2`

if [[ $flag == 0 ]]; then 
		ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'rm -rf /tmp/hc.txt'
		scp $logdir/hc.txt root@$target:/tmp/hc.txt
else
		sed -i 's/flag=0/flag=1/' $logdir/hc.txt
fi 
	
# Creates the directory for storing logs
if [ ! -f /$PWD/logs/$chm_inm ]; then echo "$logdir not found! Creating ...." ; mkdir -p $PWD/logs/$chm_inm ; fi

# Creates a placeholder file to trigger flags 
if [ ! -f $logdir/hc.txt ]; then	echo "Creating $logdir/hc.txt ..." ; touch $logdir/hc.txt ; echo $chm_inm >> $logdir/hc.txt ; 
	echo "flag=0" >> $logdir/hc.txt
fi  

# This section on-wards checks if the machine is reachable.
# If it is unreachable, a separated logfile is generated using $logfile.DEAD 
# 	extension.

function do_hc () {
	ping -w 10 -c 5 $target >/dev/null
	RETVAL=$?
		if [[ $RETVAL == 1 ]]; then
			echo "Tracing network route to $newline $target" | tee -a $logfile.DEAD
			traceroute -m 12 $target | tee -a $logfile.DEAD
			ERROR "WARNING! $newline is DEAD!"
			echo "$line is not a *NIX box or sshd is dead - check via console"
		else
			case $option_flag in
				simple)
				do_simple_hc
				;;

				full)
				do_full_hc
				;;
				
				clean)
				do_housekeeping
				;;
				
				*)
				echo "No options specified ... Exiting"
				exit 1
				;;
			esac
		fi
}

# This section performs a full battery of health checks on listed assets
#	with special checks for Windows VWS assets

function do_full_hc () {

# Identifies target flavor and injects the actual health check script if it's not present
	
	OSVER=`ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'uname'`
	
		case $OSVER in
			Linux)
			INFO Running Linux health check - $line
			ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'if [ ! -f /tmp/linux_hc.sh ]; then echo "File not found!" ; fi'
			if [ $? == 0 ] ; then 
				scp $basedir/assets/linux_hc.sh root@$target:/tmp/linux_hc.sh
				ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'chmod +x /tmp/linux_hc.sh'
			fi
			ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'sh /tmp/linux_hc.sh'
			scp root@$target:/tmp/$newline_*-$chm_inm-sysinfo-*.log $logdir
			INFO Health check completed for - $line
			;;

			HP-UX)
			INFO Running HP-UX health check - $line
			ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'if [ ! -f /tmp/hpux_hc.sh ]; then echo "File not found!" ; fi'
			if [ $? == 0 ] ; then 
				scp $basedir/assets/hpux_hc.sh root@$target:/tmp/hpux_hc.sh
				ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'chmod +x /tmp/hpux_hc.sh'
			fi
			ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'sh /tmp/hpux_hc.sh'
			scp root@$target:/tmp/$newline_*-$chm_inm-sysinfo-*.log $logdir
			INFO Health check completed for - $line
			;;

			SunOS)
			INFO Running SunOS health check - $line
			ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'if [ ! -f /tmp/sunos_hc.sh ]; then echo "File not found!" ; fi'
				if [ $? == 0 ] ; then 
					scp $basedir/assets/sunos_hc.sh root@$target:/tmp/sunos_hc.sh
					ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'chmod +x /tmp/sunos_hc.sh'
				fi
			ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'sh /tmp/sunos_hc.sh'
			scp root@$target:/tmp/$newline_*-$chm_inm-sysinfo-*.log $logdir
			INFO Health check completed for - $line
			;;

			AIX)
			INFO Running AIX health check - $line
			ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'if [ ! -f /tmp/aix_hc.sh ]; then echo "File not found!" ; fi'
			if [ $? == 0 ] ; then 
				scp $basedir/assets/aix_hc.sh root@$target:/tmp/aix_hc.sh
				ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'chmod +x /tmp/aix_hc.sh'
			fi
			ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'sh /tmp/aix_hc.sh'
			scp root@$target:/tmp/$newline_*-$chm_inm-sysinfo-*.log $logdir
			INFO Health check completed for - $line                
			;;
		esac
}

# Simple health check - see if machine up
function do_simple_hc () {
	INFO ==== Probing if $newline is up ====
	INFO CMD "date; uptime "
	ssh -q -n -o "StrictHostKeyChecking no" -o "ConnectTimeout 10" root@$target 'date ; uptime ; uname -a' | tee -a $logfile
	}

# Housekeep the logfile directory. 
function do_housekeeping () {
 INFO "Current usage of $logdir: "`du -sh . | awk '{print $1}'`
 INFO "Cleaning up ..."
 find $basedir/logs  -maxdepth 1 -type d -mtime +7 -exec tar cvzf {}.tar.gz {} \;
 find $basedir/logs -maxdepth 1 -type d -mtime +7 -exec rm -rf {} \;
 INFO "Clean up completed"
 INFO "Current usage of $logdir: "`du -sh . | awk '{print $1}'`
}


# While loop checks the asset list to see if there are any exclusions
while read -r line
	do
	newline=`echo $line | awk '{print $1}' | tr '[A-Z]' '[a-z]'`
	INFO " Resolving $newline"
	target=`nslookup $newline | grep Add | grep -v '#' | awk '{print $2}'`
		case $line in
		# enter hostnames that are exempted from the checks
		# use following format
		#
		# <matching hostname>)
		# echo "$line is exempted ... moving to next"
		# ;;

		# do not remove anything beyond this line

			*)
			do_hc
			;;
esac

#if [ ${error_flag} -eq $TRUE} ] ; then
#  exit 1
#fi

done < "$1"

exit 0
