#!/bin/bash

#HPUX health check script

chm_inm=`cat /tmp/hc.txt | head -1`
flag=`cat /tmp/hc.txt | grep flag | cut -d '=' -f2`

if [[ $flag == 0 ]]; then 
		logfile=/tmp/`hostname`_`date +%m-%d-%Y`-$chm_inm-sysinfo-pre.log
		sed -i 's/flag=0/flag=1/' /tmp/hc.txt
else 
		logfile=/tmp/`hostname`_`date +%m-%d-%Y`-$chm_inm-sysinfo-post.log
		rm -rf /tmp/*-$chm_inm-sysinfo-pre.log 
fi

#Clearing old logs 
rm -rf /tmp/*-sysinfo-post.log

INFO () {
  echo "----------------------------------------------------------------------- " | tee -a $logfile
  echo "INFO: $@" | tee -a $logfile
}

INFO2 () {
  echo "----------------------------------------------------------------------- " | tee -a $logfile2
  echo "INFO2: $@" | tee -a $logfile2
}

ERROR () {
  echo "ERROR: $@" | tee -a $logfile
  error_flag=${TRUE}
}

# HP-UX specific health checks

INFO ==== Network Details ====
INFO CMD: "netstat -in | cut -c1-47"
 netstat -in | cut -c1-47 | tee -a $logfile

INFO CMD: "netstat -rn"
 netstat -rn | tee -a $logfile

for i in $( "echo \$(lanscan | grep UP | awk {print \$3})");
do
ifconfig lan$i | tee -a $logfile
lanadmin -x -i $i | tee -a $logfile
done

INFO ==== File systems ====
INFO CMD: "bdf"
 bdf | tee -a $logfile

INFO CMD: "mount | cut -d\" \" -f1-4"
 mount | cut -d" " -f1-4 | tee -a $logfile
INFO CMD: "mount | wc -l"
 mount | wc -l | tee -a $logfile

INFO CMD: " vgdisplay -v"
 vgdisplay -v | tee -a $logfile

INFO CMD: "cat fstab "
 cat /etc/fstab | tee -a $logfile

INFO ==== Memory usage ====
INFO CMD: "dmseg | grep -i physical ; swapinfo -tam"
 dmseg | grep -i physical ; swapinfo -tam | tee -a $logfile
 
INFO ==== Look at last 25 errors on log files ====
INFO CMD:"egrep 'warn|error|fail' /var/adm/syslog/syslog.log | head -25"
 egrep 'warn|error|fail' /var/adm/syslog/syslog.log | head -25 | tee -a $logfile

INFO Health check completed
echo "Review logs at $logfile" 

exit 0
