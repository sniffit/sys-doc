!/bin/bash

#AIX health check script

chm_inm=`cat /tmp/hc.txt | head -1`
flag=`cat /tmp/hc.txt | grep flag | cut -d '=' -f2`

if [[ $flag == 0 ]]; then 
		logfile=/tmp/`hostname`_`date +%m-%d-%Y`-$chm_inm-sysinfo-pre.log
		sed -i 's/flag=0/flag=1/' /tmp/hc.txt
else 
		logfile=/tmp/`hostname`_`date +%m-%d-%Y`-$chm_inm-sysinfo-post.log
		rm -rf /tmp/*-$chm_inm-sysinfo-pre.log 
fi

#Clearing old logs 
rm -rf /tmp/*-sysinfo-post.log

INFO () {
  echo "----------------------------------------------------------------------- " | tee -a $logfile
  echo "INFO: $@" | tee -a $logfile
}

INFO2 () {
  echo "----------------------------------------------------------------------- " | tee -a $logfile2
  echo "INFO2: $@" | tee -a $logfile2
}

ERROR () {
  echo "ERROR: $@" | tee -a $logfile
  error_flag=${TRUE}
}

# AIX specific health checks
INFO ==== Network details ====
INFO CMD:"/sbin/ifconfig -a "
 ifconfig -a | tee -a $logfile

INFO CMD: "netstat -in | cut -c1-47"
 netstat -in | cut -c1-47 | tee -a $logfile

INFO CMD: "netstat -rn"
 netstat -rn | tee -a $logfile

INFO CMD: "netstat -v | egrep Ethernet|Hardware|Link|Media"
 netstat -v | egrep Ethernet|Hardware|Link|Media | tee -a $logfile

INFO ==== File systems ====
INFO CMD: "df -gt "
 df -gt | tee -a $logfile

INFO CMD: "mount | cut -d\" \" -f1-4"
mount | cut -d" " -f1-4 | tee -a $logfile
#mount | tee -a $logfile
INFO CMD: "mount | wc -l"
 mount | wc -l | tee -a $logfile

INFO CMD: "lsvg -o"
 lsvg -o | tee -a $logfile
INFO CMD: "lspv | grep inactive "
 lspv | grep inactive | tee -a $logfile

INFO ==== Memory usage ====
INFO CMD: "lsattr -El mem0"
 lsattr -El mem0 | tee -a $logfile
INFO CMD: "lsps -a"
 lsps -a| tee -a $logfile

INFO ==== Check for Errors ====
INFO Checking for permenant errors
INFO CMD: "errpt -T PERM | head -12"
 errpt -T PERM | head -25 | tee -a $logfile
 
INFO Checking for hardware errors
INFO CMD: "errpt -d H | head -12"
 errpt -d H | head -25 | tee -a $logfile
 
INFO Checking for last 12 general errors
INFO CMD: "errpt | head -12"
 errpt | head -10 | tee -a $logfile

INFO Health check completed
echo "Review logs at $logfile"

exit 0
