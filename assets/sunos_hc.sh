#!/bin/bash

#SunOS health check script

chm_inm=`cat /tmp/hc.txt | head -1`
flag=`cat /tmp/hc.txt | grep flag | cut -d '=' -f2`

if [[ $flag == 0 ]]; then 
		logfile=/tmp/`hostname`_`date +%m-%d-%Y`-$chm_inm-sysinfo-pre.log
		TMP=`mktemp /tmp/hc.txt.XXXX`
		sed '$ s/flag=0/flag=1/g' /tmp/hc.txt > "$TMP" && mv "$TMP" /tmp/hc.txt
else 
		logfile=/tmp/`hostname`_`date +%m-%d-%Y`-$chm_inm-sysinfo-post.log
		rm -rf /tmp/*-$chm_inm-sysinfo-pre.log 
fi

#Clearing old logs 
rm -rf /tmp/*-sysinfo-post.log

INFO () {
  echo "----------------------------------------------------------------------- " | tee -a $logfile
  echo "INFO: $@" | tee -a $logfile
}

INFO2 () {
  echo "----------------------------------------------------------------------- " | tee -a $logfile2
  echo "INFO2: $@" | tee -a $logfile2
}

ERROR () {
  echo "ERROR: $@" | tee -a $logfile
  error_flag=${TRUE}
}
#special for SunOS only - required to enable memstat output
sun_ver=`uname -r`

INFO ==== Network details ====
INFO CMD:"ifconfig -a"
 ifconfig -a | tee -a $logfile

INFO CMD: "netstat -in | cut -c1-47"
 netstat -in | cut -c1-47 | tee -a $logfile

INFO CMD: "netstat -rn"
 netstat -rn | tee -a $logfile

INFO ==== Services that need attention ====
INFO CMD: "svcs -a | grep maintenance"
 svcs -a | grep maintenance | tee -a $logfile

INFO ==== File systems ====
# This portion maybe a bit buggy. not sure why the return value is messed up.
df -n | head -1 | awk {print \$3} >/dev/null
FS_TYPE=$?
if [[ $FS_TYPE == zfs ]]; then
         zfs list | tee -a $logfile
         zpool list | tee -a $logfile
        else
         metastat -p | tee -a $logfile
fi

INFO CMD: " df -h"
 df -h | tee -a $logfile

INFO CMD: "mount | cut -d\" \" -f1-4"
 mount | cut -d" " -f1-4 | tee -a $logfile
INFO CMD: "mount | wc -l"
 mount | wc -l | tee -a $logfile

INFO ==== Memory usage ====
INFO CMD: "prtdiag -v | grep Memory"
 prtdiag -v | grep Memory | tee -a $logfile
INFO CMD: "prtconf | grep Memory"
 prtconf | grep Memory | tee -a $logfile

if [[ $sun_ver == 5.10 ]]; then 
	INFO CMD: "echo "::memstat" | mdb -k"
	echo "::memstat" | mdb -k | tee -a $logfile
fi

INFO CMD: "swap -l"
  swap -l | tee -a $logfile
INFO CMD: "swap -s"
  swap -l | tee -a $logfile

INFO Health check completed
echo "Review logs at $logfile" 

INFO ==== Look at last 25 errors on log files ====
INFO CMD:"egrep 'warn|error|fail' /var/adm/messages | head -25"
	egrep 'warn|error|fail' /var/adm/messages | head -25 | tee -a $logfile

exit 0
